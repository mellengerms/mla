window.onload = function() {
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    document.body.className += isOpera ? ' opera' : '';

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';
    document.body.className += isFirefox ? ' firefox' : '';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
    document.body.className += isSafari ? ' safari' : '';

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    document.body.className += isIE ? ' ie' : '';

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    document.body.className += isEdge ? ' edge' : '';

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    document.body.className += isChrome ? ' chrome' : '';
};