(function ($, Drupal) {

    window.initMap = function () { };

    Drupal.behaviors.hidePlatformNav = {
        attach: function (context, settings) {
            Drupal.resetHidePlatformNav();
            $(window).on('mousemove keyup', function () {
                Drupal.resetHidePlatformNav();
            });
        }
    };

    Drupal.hidePlatformNavT = false;

    Drupal.resetHidePlatformNav = function () {
        clearTimeout(Drupal.hidePlatformNavT);
        $('body').removeClass('js-hide-platform-nav');
        Drupal.hidePlatformNavT = setTimeout(function () {
            $('body').addClass('js-hide-platform-nav');
        }, 5000);
    }

    Drupal.initSlideTheme = function (e) {
        $('body').attr('data-slide-h',e.indexh);
        $('body').attr('data-slide-v',e.indexv);

        if ($(e.currentSlide).hasClass('slide--white')) {
            $('body').addClass('inverted');
        }
        else {
            $('body').removeClass('inverted');
        }
        if ($(e.currentSlide)[0].hasAttribute('data-bkg-color')) {
            document.body.style.backgroundColor = $(e.currentSlide).attr('data-bkg-color');
        }
        else {
            document.body.style.backgroundColor = '';
        }

        //Remove active bkg
        var $bkg = $('.slide-bkg-image');
        $bkg.appendTo('body').fadeOut(Drupal.eta, function () {
            $(this).remove();
        });

        //Add new bkg
        if ($(e.currentSlide)[0].hasAttribute('data-bkg-image')) {
            var $bkg = $('<div class="slide-bkg-image" style="background-image: url(' + $(e.currentSlide).attr('data-bkg-image') + ')"></div>');
            $bkg.prependTo('body').hide().fadeIn(Drupal.eta);
        }
    };

    Drupal.behaviors.revealCallbacks = {
        attach: function (context, settings) {
            if ($('.slides', context).length > 0) {
                Reveal.initialize({
                    controls: false,
                    progress: true,
                    center: true,
                    hash: true,
                    navigationMode: 'linear',
                    transition: 'slide',
                    width: "75%",
                    height: "100%",
                    margin: 0,
                    minScale: 1,
                    maxScale: 1,
                    appearance: {
                        baseclass: 'animated',
                        visibleclass: 'in',
                        hideagain: true,
                        delay: 300
                    },
                    dependencies: [
                        {
                            src: '/themes/custom/mla_theme/js/vendor/reveal/plugin/transit/transit.js',
                            async: false
                        },
                        {
                            src: '/themes/custom/mla_theme/js/vendor/reveal/plugin/appearance/appearance.js',
                            async: false
                        }]
                });

                Drupal.$revealSubmenu = $('<ul class="revealSubMenu" />');
                $('body').append(Drupal.$revealSubmenu);


                Reveal.addEventListener('ready', function (e) {
                    Drupal.initSlideTheme(e);
                });
                Reveal.addEventListener('slidechanged', function (e) {
                    // console.log(e);
                    Drupal.revealTitleHeight(e.currentSlide);
                    Drupal.initRevealVerticalSlider(e);

                    // Display the logo only for the first and the last slide.
                    var parent = e.currentSlide.parentNode;
                    var title = e.currentSlide.dataset.title;
                    // If slides are nested, we need to get the grandparent.
                    var currentContainer = e.currentSlide;
                    if (typeof (title) === 'undefined') {
                        var parent = e.currentSlide.parentNode.parentNode;
                        currentContainer = e.currentSlide.parentNode;
                    }

                    var index = Array.prototype.indexOf.call(parent.children,
                        currentContainer);
                    var logo = document.querySelector('.slider-logo');
                    if (index !== 0 && index !== parent.childElementCount - 1) {
                        if (!logo.classList.contains('visible')) {
                            document.querySelector('.slider-logo').classList.add('visible');
                        }
                    } else {
                        if (logo.classList.contains('visible')) {
                            document.querySelector('.slider-logo').classList.remove('visible');
                        }
                    }
                    if ($(e.currentSlide)[0].hasAttribute('data-hide_logo')) {
                        $('body .slider-logo').addClass('hidden');
                    }
                    else {
                        $('body .slider-logo').removeClass('hidden');
                    }

                    // Add a class on the reveal slider when we are on an svg
                    // slide.
                    if (e.currentSlide.querySelector('.js-svg-pan-zoom') !== null) {
                      document.body.classList.add("svg-slide");
                        var svg = e.currentSlide.querySelector('.js-svg-pan-zoom');
                        Drupal.svgPanZoom.createElement(svg);

                        // The controls are outside of the screen by default.
                        var controls = document.querySelector('#svg-pan-zoom-controls');
                        var transform = controls.transform.baseVal.getItem(0);
                        var mat = transform.matrix;
                        mat.f = mat.f - 30;
                        transform.setMatrix(mat);
                    }
                    else {
                      document.body.classList.remove("svg-slide");
                    }

                    // Refresh the bottom menu.
                    document.querySelector('.rsBullet.rsNavSelected').classList.remove('rsNavSelected');
                    document.querySelector('.rsBullet[href="#/' + index + '"]').classList.add('rsNavSelected');

                    // If slides are nested, we need to get the title on the parent.
                    if (typeof (title) === 'undefined') {
                        title = e.currentSlide.parentNode.dataset.title;
                    }
                    document.querySelector('.rsCaption').innerHTML = title;

                    // Show title when hover menu item.
                    var menuItems = document.querySelectorAll('.rsBullet');
                    for (var i = 0; i < menuItems.length; ++i) {
                        menuItems[i].addEventListener("mouseenter", function () {
                            document.querySelector('.rsCaption').innerHTML = this.dataset.title;
                        });
                        menuItems[i].addEventListener("mouseleave", function () {
                            var current = document.querySelector('.slides .present').dataset.title;
                            document.querySelector('.rsCaption').innerHTML = current;
                        });
                    }

                    Drupal.initSlideTheme(e);
                });
                // Reveal.addEventListener('fragmentshown', function (e) {
                //     Drupal.initRevealFragment(e);
                // });
                // Reveal.addEventListener('fragmenthidden', function (e) {
                //     Drupal.initRevealFragment(e);
                // });
            }
        }
    };

    Drupal.behaviors.resizeTitleHeight = {
        attach: function (context, settings) {
            $(window).resize(function() {
                Drupal.revealTitleHeight($('section.present'));
            })
        }
    }

    Drupal.revealTitleHeight = function(context) {
        var h = 0;
        var s = '.slide-title';
        for (var i = 1; i <= 6; i++) {
            s += ', .field--name-field-list-item-text h' + i + ':first-child';
        }
        $(s, context).each(function() {
            $(this).removeAttr('style');
            if ($(this).height() > h) {
                h = $(this).height();
            }
        }).height(h);
    }

    Drupal.initRevealVerticalSlider = function (e) {
        var $par = $(e.currentSlide).parent();

        if (!$par.hasClass('slides') && $('> section', $par).length > 1) { //This slide has multiple slides
            var next = !$(e.previousSlide).hasClass('future');
            Drupal.$revealSubmenu.empty();

            var x = 0;
            $('> section', $par).each(function () {
                var attr = 'data-slideh="' + e.indexh + '" data-slidev="' + x + '"';
                // if ($('.fragment', this).length > 0) {
                //     Drupal.$revealSubmenu.append('<li ' + attr + ' data-slidef="-1"><span>Market Intelligent</span></li>');

                //     var i = 0;
                //     $('.fragment', this).each(function () {
                //         Drupal.$revealSubmenu.append('<li ' + attr + ' data-slidef="' + i + '"><span>' + $(this).prev().text() + '</span></li>');
                //         i++;
                //     });
                //     $('li[data-slidev="' + e.indexv + '"]:' + (next ? 'first' : 'last')).addClass('js-active');
                // }
                // else {
                var $li = $('<li ' + attr + '><span>' + $('h1, h2, h3, h4, h5, h6', this).eq(0).text() + '</span></li>');
                if (x == e.indexv) {
                    $li.addClass('js-active');
                }
                Drupal.$revealSubmenu.append($li);
                // }
                x++;
            });

            Drupal.$revealSubmenu.fadeIn(300);
            $('li', Drupal.$revealSubmenu).click(function (e) {
                Reveal.slide(
                    ($(this)[0].hasAttribute('data-slideh') ? $(this).data('slideh') : null),
                    ($(this)[0].hasAttribute('data-slidev') ? $(this).data('slidev') : null),
                    ($(this)[0].hasAttribute('data-slidef') ? $(this).data('slidef') : null)
                );
            })
        }
        else {
            Drupal.$revealSubmenu.fadeOut(300);
        }
    };

    Drupal.initRevealFragment = function (e) {
        var $par = $(e.fragment).closest('section');
        var slidev = $par.parent().children().index($par);

        var slidef = Number($(e.fragment).attr('data-fragment-index'));

        if (e.type == 'fragmenthidden') {
            slidef -= 1;
        }

        $('li', Drupal.$revealSubmenu).removeClass('js-active');
        $('li[data-slidev="' + slidev + '"]' + (slidef != -1 ? '[data-slidef="' + slidef + '"]' : ':first'), Drupal.$revealSubmenu).addClass('js-active');
    };
})(jQuery, Drupal);
