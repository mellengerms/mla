# Content Types

## Basic Page
Full Template:
  themes/custom/mla_theme/templates/components/content/node--page.html.twig
  .node--page

## Project
Full Template:
  themes/custom/mla_theme/templates/components/content/node--project--full.html.twig
  .node--project--full

Teaser Template:
  themes/custom/mla_theme/templates/components/content/node--project--teaser.html.twig
  .node--project--teaser

Feature Small
  themes/custom/mla_theme/templates/components/content/node--project--feature-small.html.twig
  .node--project--feature-small

Feature Big
  themes/custom/mla_theme/templates/components/content/node--project--feature-big.html.twig
  .node--project--feature-big

## Hero
Full Template:
  themes/custom/mla_theme/templates/components/content/node--hero.html.twig
  .node--hero

## Article
Full Template:
  themes/custom/mla_theme/templates/components/content/node--article--full.html.twig
  .node--article--full

Teaser Template:
  themes/custom/mla_theme/templates/components/content/node--article--teaser.html.twig
  .node--article--teaser

Feature Big
  themes/custom/mla_theme/templates/components/content/node--article--feature-big.html.twig
  .node--article--feature-big


## Team Member
Full Template:
  themes/custom/mla_theme/templates/components/content/node--team-member--full.html.twig
  .node--team-member--full

Teaser Template:
  themes/custom/mla_theme/templates/components/content/node--team-member--teaser.html.twig
  .node--team-member--teaser


# Paragraphs

## Container
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--accordion-list.html.twig
  .paragraph--type--container
    .layout-container // This block has the following classes defining the layout:
      .is-responsive OR .horizontal-scroll
      .show-divider
      .layout--col-full
      .layout--col-half
      .layout--col-third
      .layout--col-fourth
      .layout--col-one-two
      .layout--col-two-one
     Each row/element within a container will have the class .col

## Tab Container
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--tab-container.html.twig
  .paragraph--type--tab-container

## Callout Image
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--callout-image.html.twig
  .paragraph--type--callout-image

## Callout Text
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--callout-text.html.twig
  .paragraph--type--callout-text

## Custom Block
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--system-block.html.twig
  .paragraph--type--system-block

## Feature
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--feature.html.twig
  .paragraph--type--feature

## Header
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--header.html.twig
  .paragraph--type--header

## HTML
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--html.html.twig
  .paragraph--type--html

## Quote
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--quote.html.twig
  .paragraph--type--quote

## Serive CTA
  themes/custom/mla_theme/templates/components/paragraphs/paragraph--service-cta.html.twig
  .paragraph--type--service-cta

