(function ($, Drupal) {
	Drupal.behaviors.footerQrModal = {
		attach: function (context, settings) {
			$('#qr-call', context).click(function(e) {
				e.preventDefault();

				var $myDialog = jQuery('<img src="/themes/custom/mla_theme/images/mla-qr-code.png" />').appendTo('body');
				Drupal.dialog($myDialog, {
					
				}).showModal();

				$('#qr-full').dialog();
			});
		}
	};

	Drupal.behaviors.footer_contact_form = {
		attach: function (context, settings) {
			$('.sign-up-trig',context).click(function(e) {
				e.preventDefault();
				var $mod = $('<div class="js-modal"></div>');
				var $form = $('#block-footercontactform').clone();
				$mod.append($form);
				$('body').append($mod);

				$('.inner',$form).click(function(e) {
					e.stopPropagation();
				});

				$mod.click(function(e) {
					$mod.fadeOut(200,function() {
						$(this).remove();
					});
				});

				signup_events($mod);
				
				var t = setTimeout(function() {
					$mod.addClass('js-active');
				},200);
			});

			var signup_events = function(cont) {
				$('#SignupForm',cont).removeClass('js-form-error');
				$('#SignupForm .submit-btn',cont).click(function(e) {
					if (!$(this).prev().find('input').is(':checked')) {
						e.preventDefault();
						e.stopPropagation();

						$('#SignupForm',cont).addClass('js-form-error');
					}
					else {	
						$('#SignupForm',cont).removeClass('js-form-error');
					}
				});
			};

			signup_events(context);

			
		}
	};
})(jQuery, Drupal);