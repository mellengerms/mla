(function ($, Drupal) {
    Drupal.behaviors.accourdion_in_page = {
        attach: function (context, settings) {
            $('.accordion-toggle',context).click(function(e) {
              	e.preventDefault();
              
                var $this = $(this);
                $(this).parent().parent().find('.accordion-toggle').removeClass('js-active');

                if ($this.next().hasClass('show')) {
                    $this.next().removeClass('show');
                    $this.next().slideUp(350);
                } else {
                    $(this).addClass('js-active');
                    $this.parent().parent().find('li ul').removeClass('show');
                    $this.parent().parent().find('li ul').slideUp(350);
                    $this.next().toggleClass('show');
                    $this.next().slideToggle(350);
                }
            });
        }
    };
})(jQuery, Drupal);            