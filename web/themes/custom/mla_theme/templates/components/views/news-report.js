(function ($, Drupal) {
  Drupal.behaviors.newsReport = {
    attach: function (context, settings) {
      $('#newsfeed-views .menu a').each(function () {
        var link = $(this).attr('href');
        if (window.location.pathname === link) {
          $(this).addClass('is-active');
        }
      });
    }
  };
})(jQuery, Drupal);
