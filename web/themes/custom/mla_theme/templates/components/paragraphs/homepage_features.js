(function ($, Drupal) {
	Drupal.behaviors.homepage_feature = {
		attach: function (context, settings) {
			$('.edit-queue',context).click(function(e) {
				e.preventDefault();
				var href = $(this).parent().parent().parent().find('.entityentity-subqueueedit-form > a').attr('href');
				window.location.href = href;
			})
		}
	};
})(jQuery, Drupal);