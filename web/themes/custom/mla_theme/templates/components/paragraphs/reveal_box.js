(function ($, Drupal) {
	Drupal.behaviors.reveal_box = {
		attach: function (context, settings) {
			var eta = 200;
			$('.reveal-box-header',context).click(function(e) {
				var $row = $(this).parent().parent();
				$row.toggleClass('js-open').find('.reveal-box-text').slideToggle(eta);
				$('.reveal-box-header',context).not($row).removeClass('js-open').find('.reveal-box-text').slideUp(eta);
			});
		}
	};
})(jQuery, Drupal);