(function ($, Drupal) {
	Drupal.behaviors.paragraph_scroll = {
		attach: function (context, settings) {
			var t_attr = 'data-hash-text';

			//Preprocess
			$('.path-projects .border-title', context).each(function() {
				$(this).attr(t_attr, hashString($(this).text()));
			});
			$('.paragraph--type--header',context).each(function() {
				$(this).attr(t_attr,hashString($('h3.ribbon',this).text()));
			});

			$(window,context).scroll(function() {
				scrollListener();
			});
			scrollListener(true);

			function scrollListener(init) {
				var wtop = $(window).scrollTop();
				var init = typeof init === 'undefined' ? false : init;

				$('.paragraph, .divider, .node--project--feature-big, .node--hero').each(function() {
					if (isInView(this)) {
						$(this).addClass('js-visible');
					}
					else {
						$(this).removeClass('js-visible');
					}
				});

				if (!init && !cancelHashbang()) { //Services page has its own behaviour
					var $last = false;
					// $('.paragraph--type--header').each(function() {
					$('[' + t_attr + ']').each(function() {
						if ($(this).offset().top < wtop + 100) {
							$last = $(this);
						}
					});

					if ($last) {
						var hash = $last.attr(t_attr);
						if (window.location.hash.split('/')[1] != hash) {
							pushState(hash);
						}
					}
					else {
						if (window.location.hash != '') {
							pushState(false);
						}
					}
				}
			}
		}
	};
})(jQuery, Drupal);
