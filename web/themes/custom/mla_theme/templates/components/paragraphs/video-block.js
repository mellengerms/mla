(function ($, Drupal) {

	Drupal.behaviors.vimeo_slider = {
		attach: function (context, settings) {
			$('.video-container',context).each(function() {
				$v = $('.vimeo-player',this);
				Drupal.play_toggle($('svg',this));
			});
		}
	};
	Drupal.play_toggle = function(subj) {
		var flip = $(subj).hasClass('js-flip');
		$(subj).toggleClass('js-flip');

		var pause = "M11,10 L18,13.74 18,22.28 11,26 M18,13.74 L26,18 26,18 18,22.28";
		var play = "M11,10 L17,10 17,26 11,26 M20,10 L26,10 26,26 20,26";
		var $animation = $('#animation',subj);
		$animation.attr({
			"from": flip ? pause : play,
			"to": flip ? play : pause
		}).get(0).beginElement();
	}
})(jQuery, Drupal);