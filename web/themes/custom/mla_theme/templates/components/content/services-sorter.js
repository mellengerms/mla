(function ($, Drupal) {
	Drupal.behaviors.services_sorter = {
		attach: function (context, settings) {
			if ($('body', context).hasClass('path-services')) {
				var $ul_c = $('<ul id="services-sort-control"></ul>'),
					$ul = $('<ul id="services-sort-list"></ul>'),
					// $li = $('<li></li>'),
					li_t = '',
					attr = 'data-services-link',
					t_attr = 'data-hash-text',
					i = 0,
					eta = 300;

				$('.node--page > .field--name-field-content-row > .field__item > .paragraph', context).each(function () {
					// console.log($(this).attr('class'));

					if ($(this).hasClass('paragraph--type--hero')) {
						var $after = $(this);
						$ul.insertAfter($after);
						$ul_c.insertAfter($after);
					}
					else {
						if ($(this).hasClass('paragraph--type--header')) {
							i++;

							li_t = $('h3', this).text();

							//Body
							if (typeof $li !== 'undefined') {
								var t = $('h3', this).text();
								if ($li.next()) {

									$li.append('<a href="#" ' + attr + '="' + (i - 1) + '" ' + t_attr + '="' + hashString(li_t) + '" class="button button-signpost">' + t + '<span></span></a>');
								}
								$ul.append($li);
							}

							//Reset 
							$li = $('<li data-services-slide="' + (i - 1) + '" class="services-sort-list--item"><div class="inner"></div></li>');

							//Title
							var $li_c = $('<li ' + attr + '="' + (i - 1) + '" ' + t_attr + '="' + hashString(li_t) + '"></li>');
							$li.prepend('<a href="#" ' + attr + '="' + (i - 1) + '" ' + t_attr + '="' + hashString(li_t) + '" class="accordion-trig">' + li_t + '<span></span></a>');
							$li_c.append('<a href="#" class="h5">' + i + '</a>');
							$li_c.append('<p>' + li_t + '</p>');

							$ul_c.append($li_c);
						}
						$('.inner', $li).append($(this).clone());
						// $(this).hide();
						$(this).remove();
					}
				});

				$ul.append($li); //for last one, needed

				$('[' + attr + '="0"]').addClass('js-active');
				$ul.find('> li:first').addClass('js-active');

				$('[' + attr + ']').click(function (e) {
					e.preventDefault();
					$('html, body').animate({
						scrollTop: $ul_c.offset().top,
					}, eta);

					var x = $(this).attr(attr);
					pushState($(this).attr(t_attr));

					if (isMobile()) {
						if ($('> li.js-active', $ul).attr('data-services-slide') != x) {
							$('> li.js-active', $ul).removeClass('js-active').addClass('js-fade').find(' > .inner').slideUp(eta, function () {
								$(this).removeAttr('style').parent().removeClass('js-fade');
							});

							$('> li', $ul_c).removeClass('js-active');

							$ul.find('> li:eq(' + x + ') > .inner').slideDown(eta, function () {
								$(this).removeAttr('style').parent().addClass('js-active');

								var tmp_top = $ul.find('> li:eq(' + x + ')').offset().top;
								$(window).scrollTop(tmp_top);
							});
							$ul_c.find('> li:eq(' + x + ')').addClass('js-active');
						}
					}
					else {
						$('> li.js-active', $ul).removeClass('js-active').addClass('js-fade').fadeOut(eta, function () {
							$(this).removeAttr('style').removeClass('js-fade');
						});

						$('> li', $ul_c).removeClass('js-active');

						$ul.find('> li:eq(' + x + ')').fadeIn(eta, function () {
							$(this).addClass('js-active').removeAttr('style');
						});
						$ul_c.find('> li:eq(' + x + ')').addClass('js-active');
					}
				});

				//Linking for hashbanging page
				Hashbang.map(['<page>',], function () {
					console.log(this);
					if ($('#services-sort-control [' + t_attr + '="' + this.page + '"]').length > 0) {
						var top = Number($('#services-sort-control [' + t_attr + '="' + this.page + '"]').offset().top);
						$('#services-sort-control [' + t_attr + '="' + this.page + '"]').trigger('click');
						$(window).scrollTop(top);
					}
				});

				Hashbang.map(['<page>', '<sub>',], function () {
					var $item = $('[data-hash="' + this.sub + '"]');

					if ($item.length > 0) {
						var $sub = $item.closest('.services-sort-list--item');
						//Slide
						$('.services-sort-list--item').removeClass('js-active');
						$sub.addClass('js-active');

						//Controller
						var i = $sub.attr('data-services-slide');
						$('[data-services-link]').removeClass('js-active');
						$('[data-services-link="' + i + '"]').addClass('js-active');

						var top = Number($sub.offset().top);
						$(window).scrollTop(top);
					}
				});
			}
		}
	};
})(jQuery, Drupal);