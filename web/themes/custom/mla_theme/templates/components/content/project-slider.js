(function ($, Drupal) {
	Drupal.behaviors.project_slider = {
		attach: function (context, settings) {
			if ($('body').hasClass('page-node-type-project') && $('.img-slideshow .field__item',context).length > 1) {
				
				$('.node--project--full .field--type-image',context).bxSlider({
					mode: 'fade',
					maxSlides: 1,
					pager: false,
					// adaptiveHeight: true,
					auto: true,
					pause: 5000,
					controls: true,
				});
			}
		}
	};	
})(jQuery, Drupal);