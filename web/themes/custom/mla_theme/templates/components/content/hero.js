(function ($, Drupal) {
	Drupal.behaviors.hero = {
		attach: function (context, settings) {
			//console.log('yip');
			var dur = 5000;
			var pos = [
				{ from: [3,10], to: [20,55], },
				{ from: [50,35], to: [50,0], },
				{ from: [30,60], to: [45,10], },
			];

			var i = 0;
			$('.node--hero').each(function() {
				$(this).css({ 
					backgroundPositionX: pos[i].from[0] + '%',
					backgroundPositionY: pos[i].from[1] + '%',
				});
				i++;
			});

			$('.view-hero-slider > .view-content', context).bxSlider({
				mode: 'fade',
				maxSlides: 1,
				pager: false,
				controls: false,
				auto: true,
				pause: dur,
				preloadImages: 'all',
				onSliderLoad: function(i) {  
					$('.node--hero:first').css({
						backgroundPositionX: pos[0].to[0] + '%',
						backgroundPositionY: pos[0].to[1] + '%',
					});
				},
				onSlideBefore: function($slideElement, oldIndex, newIndex) {  
					var t = setTimeout(function() {
						$('.node--hero:eq(' + oldIndex + ')').css({
							backgroundPositionX: pos[oldIndex].from[0] + '%',
							backgroundPositionY: pos[oldIndex].from[1] + '%',
						});
					},1000);

					$('.node--hero:eq(' + newIndex + ')').css({
						backgroundPositionX: pos[newIndex].to[0] + '%',
						backgroundPositionY: pos[newIndex].to[1] + '%',
					});
				},
			});
		}
	};
})(jQuery, Drupal);