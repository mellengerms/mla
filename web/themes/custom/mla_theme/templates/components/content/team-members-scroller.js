(function ($, Drupal) {
	Drupal.behaviors.team_members_scroller = {
		attach: function (context, settings) {
			if ($('body').hasClass('path-mla-vision')) {
				var eta = 300;
				var MOBILE = 720;
				var $content = $('.view-team-members > .view-content');
				var h_attr = 'data-team-member';

				$('[' + h_attr + ']').each(function() {
					$(this).attr(h_attr,hashString($(this).attr(h_attr)));
				});
				
				$('.node--team-member--teaser > .inner a',context).click(function(e) {
					e.preventDefault();
					var href = $(this).attr('href');

					//Create
					if (!($('#team-scroller').length > 0)) {
						var $scroller = $('<div id="team-scroller"></div>');
						var i = 0;
						$('.node--team-member--teaser .full').each(function() {
							var $item = $(this).clone();
							var ab = $(this).parent().attr('about');
							$item.attr('about',ab);

							$('[data-prev]',$item).attr('data-to',i-1);
							$('[data-next]',$item).attr('data-to',i+1);
							
							$scroller.append($item);

							i++;
						});

						$scroller.appendTo($content);
					}

					//Position
					var pos = $('#team-scroller > div').index($('#team-scroller > [about="' + href + '"]'));
					var pos_3 = roundThree(pos);
					
					$scroller = $('#team-scroller');

					repositionSlider($scroller,pos);
				});

				$(window).resize(function() {
					var $scroller = $('#team-scroller');
					
					if ($scroller.length > 0) {

						var w = $(window).width();
						var pos = Number($('.full',$scroller).index($('.js-cur.full',$scroller)));
						
						if (w < MOBILE && $scroller.hasClass('js-three-col')) {
							repositionSlider($scroller,pos);
						}
						else if ($scroller.hasClass('js-one-col')) {
							repositionSlider($scroller,pos);
						}

						scrollTo($scroller,pos,0);
					}
				});

				function roundThree(p) {
					return Math.ceil((p+1)/3)*3;
				}

				function scrollTo($sc,p,time) {
					time = typeof time === 'undefined' ? eta : time;
					$sc.animate({ 
						scrollLeft: $sc.width() * p,
					}, 0);

					$('[about]',$sc).removeClass('js-cur');
					$('[about]:eq(' + p + ')',$sc).addClass('js-cur');
				}

				function repositionSlider($sc,p,aft) {
					aft = typeof aft !== 'function' ? function() {} : aft;
					
					if ($(window).width() < MOBILE) {
						var p3 = p+1;
						$sc.removeClass('js-three-col').addClass('js-one-col');
					}
					else {
						var p3 = roundThree(p);
						$sc.removeClass('js-one-col').addClass('js-three-col');
					}

					if ($sc.attr('data-round') != p3) {
						var $after = $('> .views-row:eq(' + (p3-1) + ')',$content);
						
						$sc.attr('data-round',p3);
						$new_scrolller = $sc.clone();
						$sc.remove();
						
						if ($after.length > 0) {
							$new_scrolller.insertAfter($after);
						}
						else {
							$after = $('> .views-row:last',$content);
							$new_scrolller.insertAfter($after);
						}
						
						$sc = $new_scrolller;
						scrollTo($sc,p);

						var bl = new Blazy({
							container: '#team-scroller',
						});
						bl.revalidate();

						//Set scroller click, only once on position
						$('[data-to]').click(function(e) {
							e.preventDefault();
							var to = Number($(this).attr('data-to'));
							
							repositionSlider($('#team-scroller'),to);
						});

						$(window).scrollTop($('#team-scroller').offset().top);
					}
					else {
						scrollTo($sc,p);
					}

					pushState('leadership-team',$('.node--team-member--teaser:eq(' + p + ')').attr(h_attr));
				}

				Hashbang.map(['<page>','<sub>'], function() {
					$('.node--team-member--teaser[' + h_attr + '="' + this.sub + '"] > .inner a:first').trigger('click');
				});
			}
		}
	};
})(jQuery, Drupal);