(function ($, Drupal) {
  Drupal.behaviors.nodeArticleFull = {
    attach: function (context, settings) {
      if ($('#article-hubspot', context).length == 0) return;

      var css = [];
      $('link[href*="mla_theme"]').each(function() {
        css.push(window.location.origin + $(this).attr('href'));
      });

      var appendCss = function ($form) {
        for (i in css) {
          $form.append('<link rel="stylesheet" href="' + css[i] + '" type="text/css" />');
        }
        $('[type="text"], [type="email"]', $form).each(function() {
          var $row = $(this).closest('.hs-form-field');
          var $label = $('label',$row);
          $(this).attr('placeholder',$label.text());
          $label.remove();
        });

        $('[type="submit"]',$form).addClass('button');
      }

      hbspt.forms.create({
        target: '#article-hubspot',
        region: "na1",
        portalId: "7763850",
        formId: "3a694ee9-0e8b-4030-a313-4d5cd3c475c0",
        onFormReady: function (e) { //Append styles to form
          appendCss($(e));
          $(e).trigger('resize');
        },
        onFormSubmitted: function (e) { //Append styles to thank you page
          appendCss($(e));
          $(e).trigger('resize');
        }
      });
    }
  };
})(jQuery, Drupal);
