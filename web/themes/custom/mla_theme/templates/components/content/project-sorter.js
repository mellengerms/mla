(function ($, Drupal) {
	Drupal.behaviors.project_sorter = {
		attach: function (context, settings) {
			var camelToDash = function(cam) {
				return cam.replace(/([a-z])([A-Z])/g,'$1-$2').toLowerCase();
			}

			var data_attr = [
				'data-status',
				'data-city',
				// 'data-province',
				'data-building-type',
				'data-product-type',
				// 'data-neighbourhood',
				'data-year-released',
			];

			var filter_sort = function($subj) {
				var $row = $subj.next();
				// $row.css({ background: 'red' });

				$('.trig',$subj).hide();

				$('.views-row.js-active > article',$row).each(function() {
					var data = $(this).data();
					
					for (var i in data) {
						$('.trig[data-' + camelToDash(i) + '="' + data[i] + '"]',$subj).show();
					}
				});
			};

			var project_sort = function($subj) {
				var data = {};
				$('.js-active',$subj).each(function() {
					var d = $(this).data();
					for (var i in d) {
						data[i] = d[i];
					}
				});
				
				var $row = $subj.next();
				$row.css({ opacity: 0 }).animate({ opacity: 1 },750); //fade back in
				
				var s_string = [];
				for (var x in data) {
					s_string.push('[data-' + camelToDash(x) + '="' + data[x] + '"]');
				}
				s_string = s_string.join('');

				$('.node--project--teaser',$row).each(function() {
					$(this).parent().hide().removeClass('js-active');
				});

				// var filters = {};
				$('.node--project--teaser' + s_string,$row).each(function() {
					$(this).parent().show().addClass('js-active');
				});
			}


			if ($('body',context).hasClass('path-projects')) {
				var $rows = $('.view-display-id-block_2, .view-display-id-block_3',context);
				
				$rows.each(function() {
					var $ul = $('<ul class="project-sort-filters"></ul>');
					var $cur = $(this);
					var data = {};
					$('.node--project--teaser',this).each(function() {
						for (var i in data_attr) {
							if (!($cur.hasClass('view-display-id-block_3') && data_attr[i] == 'data-status') && !($cur.hasClass('view-display-id-block_2') && data_attr[i] == 'data-year-released')) {
								var t = data_attr[i].replace('data-','');
								if (typeof data[t] === 'undefined') {
									data[t] = [];
								}

								var v = $(this).attr(data_attr[i]);
								if (v != '') {
									data[t].push(v);
								}
							}
						}
					});

					for(var x in data) {
						data[x] = data[x].filter(onlyUnique);
						
						if (data[x].length > 0) {
							var $li = $('<li data-' + x + '></li>');
							
							//Title
							var t = x.split('-').join(' ');

							var $li_t = $('<span>' + (t) + '</span>');
							var $li_ul = $('<ul></ul>');

							for (var i in data[x]) {
								$li_ul.append('<li class="trig" data-' + x + '="' + data[x][i] + '">' + data[x][i] + '</li>'); 
							}
							$li.append($li_t).append($li_ul);

							$ul.append($li);
						}
					}
					
					$ul.append('<li class="reset"><span>Reset</span></li>');
					$(this).prepend($ul);

					$('span',$ul).click(function() {
						if ($(this).parent().hasClass('reset')) {
							$('li',$ul).removeClass('js-active');
							project_sort($ul);
							filter_sort($ul);
						}
						else {	
							$ul.toggleClass('js-open');
						}
					});
					$('.trig',$ul).click(function(e) {
						if ($(this).hasClass('js-active')) {

						}
						$(this).parent().children().not(this).removeClass('js-active');
						$(this).toggleClass('js-active');

						project_sort($ul);
						filter_sort($ul);
					});
				});
			}
		}
	};
})(jQuery, Drupal);