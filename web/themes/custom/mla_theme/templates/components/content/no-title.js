(function ($, Drupal) {
  Drupal.behaviors.modal_no_title = {
    attach: function (context, settings) {
      $('.use-ajax[data-dialog-type="modal"]', context).on('click', function() {
        if ($(this).hasClass('no-title')) {
          $('body').addClass('modal-no-title');
        }
        else {
          $('body').removeClass('modal-no-title');
        }
      });
    }
  };
})(jQuery, Drupal);
