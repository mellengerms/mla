window.initMap = function() {
	(function ($, Drupal) {
		if ($('body').hasClass('page-node-type-project') || $('.picatic-map').length > 0) {
			// console.log('ss');

			var pos = { lat: 49.269640, lng: -123.103439 };
			var map = new google.maps.Map(document.getElementById('g-map'), {
			center: pos,
			scrollwheel: false,
			zoom: 12,
			styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#dbdbdb"},{"visibility":"on"}]}]
			});

			var x_mark = {
				path: 'M35.412,40.409c0-5.588-4.53-10.118-10.118-10.118c-5.588,0-10.118,4.53-10.118,10.118 c0,1.172,0.21,2.293,0.576,3.34H15.72l9.948,26.93l9.2-26.93h-0.032C35.202,42.702,35.412,41.581,35.412,40.409z M25.205,44.667 c-2.28,0-4.128-1.848-4.128-4.128c0-2.28,1.848-4.128,4.128-4.128c2.28,0,4.128,1.848,4.128,4.128 C29.333,42.819,27.485,44.667,25.205,44.667z',
    			fillColor: '#010101',
				fillOpacity: 1,
				scale: 1.5,
				strokeWeight: 0,
				anchor: new google.maps.Point(15, 65),
			};

			var fields = [
				'data-street',
				// 'data-neighborhood',
				'data-city',
				'data-province',
				'data-postal',
			];

			var address = [];
			for (var i in fields) {
				var v = $('#g-map').attr(fields[i]);
				if (v != '') {
					address.push(v);
				}
			}
			address.push('Canada');
			address = address.join(', ');
			// console.log(address);
			
			var geocoder = new google.maps.Geocoder();
				geocoder.geocode({
				'address': address
			}, 
			function(results, status) {
				if(status == google.maps.GeocoderStatus.OK) {
					new google.maps.Marker({
						icon: x_mark,
						position: results[0].geometry.location,
						map: map
					});
					map.setCenter(results[0].geometry.location);
				}
			});
		}
	})(jQuery, Drupal);
};