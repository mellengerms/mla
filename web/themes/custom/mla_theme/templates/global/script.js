(function ($, Drupal) {
	Drupal.behaviors.redirects = {
		attach: function (context, settings) {
			if ($('body', context).hasClass('page-node-type-team-member')) {
				window.location.href = '/mla-vision#!/leadership-team/' + window.location.pathname.split('/')[2];
			}
		}
	};
	Drupal.behaviors.blazy = {
		attach: function (context, settings) {
			var bLazy = new Blazy({
				offset: 2000,
				selector: '[data-src]:not(.lazy)',
				src: 'data-src',
				error: function (e, msg) { }
			});

			var bLazyBkg = new Blazy({
				offset: 2000,
				selector: '[data-bkg-src]',
				src: 'data-bkg-src',
				error: function (e, msg) { }
			});

			$('.field--name-field-profile-image-wide img, .image-style-projects-slider').each(function () {
				$(this).attr('src', $(this).attr('data-src'));
			});
			$('.paragraph--type--service-cta').each(function () {
				$(this).css({
					backgroundImage: 'url(' + $(this).attr('data-bkg-src') + ')',
				});
			});
		}
	};

	Drupal.behaviors.hashbang = {
		attach: function (context, settings) {
			var scrollTo = function (url) {
				var t = setTimeout(function () {
					if ($('[data-hash-text="' + url.page + '"]').length > 0) {
						$(window).scrollTop($('[data-hash-text="' + url.page + '"]').offset().top);
					}
				}, 10);
			};

			//Services page has its own behaviour
			//Team members page has its own behaviour
			if (!cancelHashbang()) {
				Hashbang.map(['<page>',], function () {
					scrollTo(this);
				});
				Hashbang.map(['<page>', '<sub>'], function () {
					scrollTo(this);
				});
			}
		}
	};

	Drupal.behaviors.closeDialog = {
		attach: function (context, settings) {
			$('body').on('click', '.ui-widget-overlay', function () {
				// console.log('hey');
				$('#drupal-modal').dialog('close');
			});
			// $('.ui-widget-overlay').click(function () {
			//  console.log('yu');
			// $('.ui-dialog').dialog('close');
			// })
		}
	};

	Drupal.behaviors.responsiveIframe = {
		attach: function (context, settings) {
			window.addEventListener("message", function(event) {
				// Do we trust the sender of this message?  (might be
				// different from what we originally opened, for example).
				if (event.origin !== "https://mlarealty.ca") {
					return;
				}
				console.log(event.data);
				if (document.getElementById('sizetracker') !== null && Number.isInteger(event.data)) {
					document.getElementById('sizetracker').height = event.data + 50;
				}
			}, false);

			if (document.getElementById('sizetracker') !== null) {
				setTimeout(function () {
					document.getElementById('sizetracker').contentWindow.postMessage('height', '*');
				}, 1000);
			}

			function updatetIframeWindowSize() {
				document.getElementById('sizetracker').contentWindow.postMessage('height', '*');
			}

			window.addEventListener('resize', updatetIframeWindowSize);

		}
	};

})(jQuery, Drupal);

function dev() {
	return window.location.href.indexOf('local.mla') > -1;
}

function browser() {
	var is = {
		chrome: navigator.userAgent.indexOf('Chrome') > -1,
		explorer: navigator.userAgent.indexOf('MSIE') > -1,
		firefox: navigator.userAgent.indexOf('Firefox') > -1,
		safari: navigator.userAgent.indexOf("Safari") > -1,
		opera: navigator.userAgent.toLowerCase().indexOf("op") > -1,
	};

	if (is.chrome && is.safari) {
		is.safari = false;
	}
	if (is.chrome && is.opera) {
		is.chrome = false;
	}

	return is;
}

function onlyUnique(value, index, self) {
	return self.indexOf(value) === index;
}
function isMobile() {
	return jQuery(window).width() < 720;
}
function hashString(str) {
	return str.trim().toLowerCase().replace(/\s+/g, '-').replace('&', 'and');
}
function pushState(page, sub) {
	page = typeof page === 'undefined' ? false : page;
	sub = typeof sub === 'undefined' ? false : sub;

	if (!page) {
		history.pushState({}, '', ' ');
	}
	else {
		var url = [page];
		if (sub) {
			url.push(sub);
		}

		history.pushState({}, '', '#!/' + url.join('/'));
	}
}
function cancelHashbang() {
	return jQuery('body').hasClass('path-services') || jQuery('body').hasClass('path-mla-vision');
}

function isInView(row) {
	var container = jQuery(window);
	var elementTop = jQuery(row).offset().top,
		elementHeight = jQuery(row).height(),
		containerTop = container.scrollTop(),
		containerHeight = container.height();

	return ((((elementTop - containerTop) + elementHeight) > 0) && ((elementTop - containerTop) < containerHeight));
}
