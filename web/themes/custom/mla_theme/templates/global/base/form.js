(function ($, Drupal) {
  Drupal.behaviors.placeHolder = {
    attach: function (context) {
      $('.form-item--text-input input, .form-type-textarea textarea', context).each(function () {
        if ($(this).val() != '' || $(this).attr('value') != '') {
          $(this).closest('.form-item').addClass('js-focus');
        }
      });

      $('.form-item--text-input input, .form-type-textarea textarea', context).focusout(function () {
        if ($(this).val() == '') {
          $(this).closest('.form-item').removeClass('js-focus');
        }
      }).focus(function () {
        $(this).closest('.form-item').addClass('js-focus');
      });
    }
  };
})(jQuery, Drupal);