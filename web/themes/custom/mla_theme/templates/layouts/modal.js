(function ($, Drupal) {
	Drupal.behaviors.modalTriggerClass = {
		attach: function (context, settings) {
			$('.webform-dialog', context).on('mousedown', function (e) {
			});
		}
	};

	Drupal.behaviors.modalUserFix = {
		attach: function (context, settings) {
			if ($('.node--team-contact--full').length > 0) {
				var t = setTimeout(function () {
					$('.ui-dialog').addClass('contact-modal');
				}, 10);
			}

			// clearTimeout(Drupal.modalContactFixT);
			// Drupal.modalContactFixT = setTimeout(function () {
			// 	$(window).trigger('resize');
			// 	Drupal.modalUserResize();
			// }, 10);
			// $(window).resize(function () {
			// 	clearTimeout(Drupal.modalContactFixT);
			// 	Drupal.modalContactFixT = setTimeout(function () {
			// 		Drupal.modalUserResize();
			// 	}, 10);
			// });
		}
	};

	// Drupal.modalUserResize = function() {

	// }


	Drupal.behaviors.modalGalleryFix = {
		attach: function (context, settings) {
			clearTimeout(Drupal.modalGalleryFixT);
			Drupal.modalGalleryFixT = setTimeout(function () {
				$(window).trigger('resize');
				Drupal.modalGalleryResize();
			}, 10);
			$(window).resize(function () {
				clearTimeout(Drupal.modalGalleryFixT);
				Drupal.modalGalleryFixT = setTimeout(function () {
					Drupal.modalGalleryResize();
				}, 10);
			})
		}
	};

	Drupal.modalGalleryResize = function () {
		var $modal = $('.node--gallery--full:first').closest('.ui-dialog');

		if ($modal.length == 0) {
			return;
		}

		$modal.addClass('gallery-modal');
		var h_av = $('#drupal-modal', $modal).height() - $('.node--gallery--full > h6', $modal).outerHeight(true) - 70;
		var w_av = $('.media--image:first', $modal).width() - (parseInt($('.media--image:first', $modal).css('paddingTop')) * 2);

		$('img', $modal).each(function () {
			$(this).removeAttr('style');

			var ratio = $(this).attr('width') / $(this).attr('height');
			var pratio = w_av / h_av;
			if (ratio < pratio) {
				var css = { width: 'auto', height: h_av };
			}
			else {
				var css = { width: w_av, height: 'auto' };
			}

			$(this).css(css);
		});
	};
})(jQuery, Drupal);