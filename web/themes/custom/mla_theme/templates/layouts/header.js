(function ($, Drupal) {
	Drupal.behaviors.false_flag = {
		attach: function (context, settings) {
		
			//projects
			if ($('body').hasClass('page-node-type-project')) {
				$('a[data-drupal-link-system-path="node/44"]').addClass('is-active');
			}
			//newsfeed
			if ($('body').hasClass('page-node-type-article')) {
				$('a[data-drupal-link-system-path="newsfeed"]').addClass('is-active');
			}

		}
	};
	Drupal.behaviors.hamburger = {
		attach: function (context, settings) {

			$('#block-mla-theme-main-menu',context).click(function(e) {
				if ($('body').hasClass('js-hamburger-open')) {
					$('body').removeClass('js-hamburger-open');
				}
			});
			
			$('#block-mla-theme-main-menu .menu',context).click(function(e) {
				$('body').toggleClass('js-hamburger-open');
				e.stopPropagation();
			});
			$('#block-mla-theme-main-menu .menu > li',context).click(	function(e) {
				e.stopPropagation();
			});
		}
	};
})(jQuery, Drupal);