var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var watch = require('gulp-watch');
var shell = require('gulp-shell');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat-multi');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var fs = require("fs");
var runSequence = require('run-sequence');
var config = require("./example.config");

/**
 * If config.js exists, load that config for overriding certain values below.
 */
function loadConfig() {
  if (fs.existsSync(__dirname + "/./config.js")) {
    config = {};
    config = require("./config");
  }

  return config;
}

loadConfig();

/**
 * This task generates CSS from all SCSS files and compresses them down.
 */
gulp.task('sass', function () {
  return gulp.src([
      './templates/global/**/*.scss',
      './templates/layouts/**/*.scss',
      './templates/components/**/*.scss',
      './templates/presentation/**/*.scss'
    ])
    .pipe(sassGlob())
    .pipe(sourcemaps.init())
    .pipe(sass({
      noCache: true,
      outputStyle: "compressed",
      lineNumbers: false,
      loadPath: './css/*',
      sourceMap: true
    })).on('error', function(error) {
      gutil.log(error);
      this.emit('end');
    })
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css'))
    // .pipe(notify({
    //   title: "SASS Compiled",
    //   message: "All SASS files have been recompiled to CSS.",
    //   onLast: true
    // }))
    ;
});

/**
 * This task minifies javascript in the js/js-src folder and places them in the js directory.
 */
gulp.task('js', function() {
  concat({
    'global.js': 'templates/global/**/*.js',
    'components.js': 'templates/components/**/*.js',
    'layouts.js': 'templates/layouts/**/*.js',
  })
    .pipe(uglify())
    .pipe(gulp.dest('js'))
    // .pipe(notify({
    //   title: "JS Compiled",
    //   message: "All JS files have been recompiled and minified.",
    //   onLast: true
    // }))
    ;
});

/**
 * Defines a task that triggers a Drush cache clear (css-js).
 */
gulp.task('drush:cc', function () {
  if (!config.drush.enabled) {
    return;
  }

  return gulp.src('', {read: false})
    .pipe(shell([
      config.drush.alias.css_js
    ]))
    .pipe(notify({
      title: "Caches cleared",
      message: "Drupal CSS/JS caches cleared.",
      onLast: true
    }));
});

/**
 * Defines a task that triggers a Drush cache rebuild.
 */
gulp.task('drush:cr', function () {
  if (!config.drush.enabled) {
    return;
  }

  return gulp.src('', {read: false})
    .pipe(shell([
      config.drush.alias.cr
    ]))
    .pipe(notify({
      title: "Cache rebuilt",
      message: "Drupal cache rebuilt.",
      onLast: true
    }));
});

/**
 * Define a task to spawn Browser Sync.
 * Options are defaulted, but can be overridden within your config.js file.
 */
gulp.task('browser-sync', function() {
  browserSync.init({
    files: ['css/**/*.css', 'js/*.js'],
    port: config.browserSync.port,
    proxy: config.browserSync.hostname,
    open: config.browserSync.openAutomatically,
    reloadDelay: config.browserSync.reloadDelay,
    injectChanges: config.browserSync.injectChanges
  });
});

/**
 * Define a task to be called to instruct browser sync to reload.
 */
gulp.task('reload', function() {
  browserSync.reload();
});

/**
 * Combined tasks that are run synchronously specifically for twig template changes.
 */
gulp.task('flush', function() {
  runSequence('drush:cr', 'reload');
});

/**
 * Defines the watcher task.
 */
gulp.task('watch', function() {
  gulp.watch(['templates/**/*.scss'], { usePolling: true }, ['sass',]);
  gulp.watch(['templates/**/*.js'], { usePolling: true }, ['js',]);
});

gulp.task('default', ['watch']);