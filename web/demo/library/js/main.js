$(function(){
 var shrinkHeader = 200;
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           $('.header-container').addClass('shrink');
        }
        else {
            $('.header-container').removeClass('shrink');
        }
  });
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }
});


$(document).ready(function() {
      var navigation = responsiveNav("#nav", {
        animate: true,        // Boolean: Use CSS3 transitions, true or false
        transition: 400,      // Integer: Speed of the transition, in milliseconds
        label: "Menu",        // String: Label for the navigation toggle
        insert: "before",      // String: Insert the toggle before or after the navigation
        customToggle: "",     // Selector: Specify the ID of a custom toggle
        openPos: "relative",  // String: Position of the opened nav, relative or static
        jsClass: "js",        // String: 'JS enabled' class which is added to <html> el
        init: function(){},   // Function: Init callback
        open: function(){
        	$("#nav a").click(function(){
        		navigation.toggle();
        		removeClass(nav, "opened");
         		addClass(nav, "closed");
         		setAttributes(nav, {"aria-hidden": "true"});
        		});
        	},   // Function: Open callback
        close: function(){}   // Function: Close callback
      });

});


$(document).ready(function() {												
	$(".fancybox").fancybox({
	padding: 0
	});				
});




$(document).ready(function() {

    (function() {

      // store the slider in a local variable
      var $window = $(window),
          flexslider;

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 768) ? 2 :
               (window.innerWidth < 960) ? 3 : 4;
      }


      $(window).load(function() {
      
        $('#projectslider').flexslider({
          animation: "slide",
          animationSpeed: 500,
          animationLoop: false,
          itemWidth: 306,
          itemMargin: 0,
          controlNav: false,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            $('body').removeClass('loading');
            flexslider = slider;
          }
        });
        


      $('#homehero').flexslider({
        animation: "slide",
        controlNav: true,
        directionNav: false,
        animationSpeed: 800,
          slideshowSpeed: 11000,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });  

  
      });
      


      // check grid size on resize event
      $window.resize(function() {
        var gridSize = getGridSize();

        //flexslider.vars.minItems = gridSize;
       // flexslider.vars.maxItems = gridSize;
      });
    }());
    


});


$(document).ready(function() {

        
      $('#developmentslider').flexslider({
        animation: "slide",
        controlNav: true,
        directionNav: false,
        animationSpeed: 500,
        after: function(slider){
        	var slider1 = $('#developmentslider').data('flexslider');
        	slider1.resize();
        }
      });
      
});

$(window).resize(function() {
  var ww = $(window).width();
  $('.flexslider ul li').each(function() {
    $(this).width(ww);
  });
});


$(function() {
    $('.row.development .col').matchHeight();
});



$(document).ready(function() {

			/* enable form validation */
			$("#SignupForm").validate(
				{
					submitHandler: function(form) {
						form.submit();
					},
					rules: {
						FirstName: "required",
						LastName: "required",
						"Emails[Primary]": {
							required: true,
							email: true
						},
						"Phones[Home]": "required",
						"Questions[34035][]": "required"
					},
					messages: {
						FirstName: "Please enter a first name",
						LastName: "Please enter a last name",
						"Emails[Primary]": {
							required: "Please enter an email address",
							email: "Email address format is not valid"
						},
						"Phones[Home]": "Please enter a phone number",
						"Questions[34035][]": "Please consent to receiving marketing messages from BLVD"
					}
				}
			);
			
$('input, textarea').placeholder();
$("select").uniform();

});



$(window).load(function(){

$('#HeardHow').bind('change',function(){
    var showOrHide = ($(this).val() == 159826) ? true : false;
    $('#HeardOther').toggle(showOrHide);
 });
 
 $('input').bind('change',function(){
    var showOrHide = ($(this).val() == XXXXXX) ? true : false;
    $('#brokerage').toggle(showOrHide);
 });
 
 
});


/*---------------------------------
	Inner Page Scroll
-----------------------------------*/

    $(document).ready(function() {

      $(document).on('click', '#servicesnav li a[href*="#"]', function() {
        var slashedHash = '#/' + this.hash.slice(1);
        if ( this.hash ) {

          if ( slashedHash === location.hash ) {
            $.smoothScroll({scrollTarget: this.hash, offset: -90});
          } else {
            $.bbq.pushState(slashedHash);
          }

          return false;
        }
      });

      $(window).bind('hashchange', function(event) {
        var tgt = location.hash.replace(/^#\/?/,'');
        if ( document.getElementById(tgt) ) {
          $.smoothScroll({scrollTarget: '#' + tgt, offset: -90});
        }
      });

      $(window).trigger('hashchange');
    });




$(document).ready(function() {

	function equalHeights (element1, element2) {
		var height;

		if (element1.outerHeight() > element2.outerHeight())
		{
			height = element1.outerHeight();
			element2.css('height', height);
		}
		else {
			height = element2.outerHeight();
			element1.css('height', height);
		}
	}

	equalHeights($('.blogleft'), $('.blogright') )

});
