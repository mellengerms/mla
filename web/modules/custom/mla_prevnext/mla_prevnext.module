<?php

/**
 * @file
 * Contains mla_prevnext.module.
 */

use Drupal\node\Entity\Node;
use \Drupal\Core\Url;

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function mla_prevnext_node_view(array &$build, \Drupal\node\Entity\Node $node, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display, $view_mode) {
  if ($node->bundle() == 'article') {
    $previous_next = __mla_getPreviousNext($node);
    if ($display->getComponent('prevnext_previous')) {
      $build['prevnext_previous'] = [
        '#theme' => 'prevnext',
        '#direction' => 'previous',
        '#text' => t('Previous'),
        '#nid' => $previous_next['prev'],
        '#url' => Url::fromUserInput('/node/' . $previous_next['prev'], ['absolute' => TRUE])->toString(),
        '#void' => empty($previous_next['prev']),
      ];
    }

    if ($display->getComponent('prevnext_next')) {
      $build['prevnext_next'] = [
        '#theme' => 'prevnext',
        '#direction' => 'next',
        '#text' => t('Next'),
        '#nid' => $previous_next['next'],
        '#url' => Url::fromUserInput('/node/' . $previous_next['next'], ['absolute' => TRUE])->toString(),
        '#void' => empty($previous_next['next']),
      ];
    }
    $build['#cache']['tags'][] = 'prevnext-' . $node->bundle();
  }
}

function __mla_getPreviousNext(Node $node) {
  $nodes = __mla_prevnext_getNodesOfType($node);
  $current_nid = $node->id();
  $current_key = array_search($current_nid, $nodes);
  $prevnext['prev'] = ($current_key == 0) ? '' : $nodes[$current_key - 1];
  $prevnext['next'] = ($current_key == count($nodes) - 1) ? '' : $nodes[$current_key + 1];

  return $prevnext;
}

function __mla_prevnext_getNodesOfType(Node $node) {
  $nodes = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('type', $node->bundle())
    ->condition('langcode', $node->language()->getId())
    ->condition('field_ee_entry_id', 0)
    ->execute();

  return array_values($nodes);
}

