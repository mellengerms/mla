<?php

namespace Drupal\video_embed_url\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Display.
   *
   * @return string
   *   Return Hello string.
   */
  public function display($provider, $video_id) {
    $autoplay = TRUE;
    $width = '800px';
    $height = '400px';



    $ra['pre'] = [
      '#markup' => '<div class="video-wrapper">',
    ];
    $ra['iframe'] = [
      '#type' => 'video_embed_iframe',
      '#provider' => $provider,
      '#url' => $this->getUrl($provider, $video_id),
      '#query' => [
        'autoplay' => $autoplay,
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
    $ra['suf'] = [
      '#markup' => '</div>',
    ];


    return $ra;
  }

  private function getUrl($provider, $id) {
    $url = '';
    switch ($provider) {
      case 'vimeo':
        $url =  sprintf('https://player.vimeo.com/video/%s', $id);
        break;

      case 'youtube':
        $url = sprintf('https://www.youtube.com/embed/%s', $id);
        break;
    }
    return $url;
  }

}
