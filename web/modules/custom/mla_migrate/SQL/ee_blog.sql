-- Find field_id in channel_fields table:
--   4 cf_page_body
--   16 cf_blog_photo
--   47 blog_video (not to migrate)

-- Run this query and export as CSV file "exp_channel_data.csv" into csv folder
SELECT DISTINCT t.`entry_id` , t.title, t.url_title, CONCAT(t.year, '-', t.month, '-', t.day) AS entry_date, d.`field_id_4` AS body, d.`field_id_16` AS blog_photo, c.`cat_id` AS category
FROM  `exp_channel_data` AS d
JOIN  `exp_channel_titles` AS t ON d.`entry_id` = t.`entry_id`
JOIN  `exp_category_posts` AS c ON d.`entry_id` = c.`entry_id`
WHERE d.channel_id =5
LIMIT 0 , 100000;