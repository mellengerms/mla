-- Find field_id in channel_fields table:
--   4 cf_page_body
--   16 cf_blog_photo
--   47 blog_video (not to migrate)
/*
Find field_id in channel_fields table:

field_id field_name                     drupal_field
 7       cf_projects_status             field_project_status
 8       cf_projects_developer          field_developer
 9       cf_projects_description        body
10       cf_projects_gallery            field_project_images
12       cf_projects_cover              field_brand_tile
13       cf_projects_url                field_registration_link
50       full_size_cover_image          field_featured_image
61       project_address                field_project_site_address
77       project_total_number_of_units  field_number_of_homes
85       project_unit_sizes             field_size
90       project_images
95       unit_sale_price                field_price
99       plan_bedrooms                  field_bedrooms
*/

-- Add http:// to all links
UPDATE exp_channel_data SET field_id_13=REPLACE(field_id_13,'http://','');
UPDATE exp_channel_data SET field_id_13=CONCAT('http://', field_id_13) WHERE field_id_1 <> '';

-- Remove line breaks
UPDATE exp_channel_data SET field_id_10 = REPLACE(field_id_10, '\r\n', ',');
UPDATE exp_channel_data SET field_id_10 = REPLACE(field_id_10, '\n', ',');
UPDATE exp_channel_data SET field_id_10 = REPLACE(field_id_10, '\r', ',');

-- Run this query and export as CSV file "exp_channel_data_projects.csv" into csv folder
-- Do NOT change the order of fields!
SELECT DISTINCT t.`entry_id` ,
                t.title,
                t.url_title,
                d.`field_id_99` AS plan_bedrooms,
                d.`field_id_61` AS project_address,
                d.`field_id_8` AS cf_projects_developer,
                d.`field_id_77` AS project_total_number_of_units,
                d.`field_id_7` AS cf_projects_status,
                d.`field_id_9` AS cf_projects_description,
                d.`field_id_95` AS unit_sale_price,
                d.`field_id_85` AS project_unit_sizes,
                d.`field_id_13` AS cf_projects_url,
                d.`field_id_12` AS cf_projects_cover,
                d.`field_id_50` AS full_size_cover_image,
                d.`field_id_10` AS cf_projects_gallery
FROM  `exp_channel_data` AS d
JOIN  `exp_channel_titles` AS t ON d.`entry_id` = t.`entry_id`
JOIN  `exp_category_posts` AS c ON d.`entry_id` = c.`entry_id`
WHERE d.channel_id =3
LIMIT 0 , 100000;

-- Get all gallery files one in each row, and export as "exp_channel_data_projects_slideshow.csv"
select DISTINCT
  exp_channel_data.entry_id,
  SUBSTRING_INDEX(SUBSTRING_INDEX(exp_channel_data.field_id_10, '\n', numbers.n), '\n', -1) cf_projects_gallery
from
  (select 1 n union all
   select 2 union all select 3 union all
   select 4 union all select 5) numbers INNER JOIN exp_channel_data
  on CHAR_LENGTH(exp_channel_data.field_id_10)
     -CHAR_LENGTH(REPLACE(exp_channel_data.field_id_10, '\n', ''))>=numbers.n-1
order by
  cf_projects_gallery