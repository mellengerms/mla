<?php

namespace Drupal\mla_sendgrid\Plugin\WebformHandler;


use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Exception;
use GuzzleHttp\Client;

/**
 * Webform SendGridEventRsvp handler.
 *
 * @WebformHandler(
 *   id = "mla_sendgrid_event_rsvp",
 *   label = @Translation("MLA SendGrid Event RSVP"),
 *   category = @Translation("External"),
 *   description = @Translation("Send confirmation email via the SendGrid API."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class SendGridEventRsvp extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
    WebformSubmissionInterface $webform_submission
  ) {

    $data = $webform_submission->getData();
    // Only process this if it is a new submission and if the person will attend.
    if ($webform_submission->isNew() && $data['attend'] == 'yes') {

      /** @var \Drupal\node\Entity\Node $node */
      $node = $webform_submission->getSourceEntity();

      $timeOptions = ['timezone' => 'America/Vancouver'];

      $time[] = $node->field_event_date_time->start_date->format('g:i A', $timeOptions);
      $endTime = $node->field_event_date_time->end_value ? $node->field_event_date_time->end_date->format('g:i A', $timeOptions) : '';
      if ($endTime && $endTime != $time[0]) {
        $time[] = $endTime;
      }


      $icalPath =  Url::fromRoute('mla_sendgrid.event_controller_ical', ['node' => $node->id()], ['absolute' => TRUE])->toString();
      $icalFile = file_get_contents($icalPath);

      $emailData = [
        'template_id'      => 'd-c2ff63c9c2994d99ab6526d4976769d3',
        'from'             => [
          'email' => 'do-not-reply@mlacanada.com',
          'name'  => 'MLA Canada',
        ],
        'personalizations' => [
          [
            'to' => [
              [
                'email' => $data['email'],
                'name'  => $data['first_name'] . ' ' . $data['last_name'],
              ],
            ],
            'dynamic_template_data' => [
              'title'     => $node->label(),
              'firstName' => $data['first_name'],
              'day'       => $node->field_event_date_time->start_date->format('l', $timeOptions),
              'date'      => $node->field_event_date_time->start_date->format('F jS, Y', $timeOptions),
              'time'      => implode(' - ', $time),
              'venue'     => $node->field_event_venue->value,
              'street'    => $node->field_event_location->address_line1,
              'city'      => $node->field_event_location->locality . ' ' . $node->field_event_location->administrative_area . ' ' . $node->field_event_location->postal_code,
              'year'      => date('Y'),
              'subject'   => $node->label(),
            ],
          ],
        ],
      ];

      if (!empty($icalFile)) {
        $emailData['attachments'] = [
          [
            'type' => 'text/calendar',
            'filename' =>'event-' . $node->id() . '.ics',
            'content' => base64_encode($icalFile)
          ]
        ];
      }

      $client = new Client();
      try {
        $token = $this->configFactory->get('sendgrid_integration.settings')
                                     ->get('apikey');

        if (empty($token)) {
          throw new Exception('No API token set for SendGrid');
        }

        $client->request('POST', 'https://api.sendgrid.com/v3/mail/send', [
          'headers' => [
            'Authorization' => 'Bearer ' . $token,
            'Content-Type'  => 'application/json',
          ],
          'body'    => json_encode($emailData),
        ]);
      } catch (Exception $e) {
        \Drupal::logger('mla_sendgrid')->error($e->getMessage());
      }

    }
  }
}