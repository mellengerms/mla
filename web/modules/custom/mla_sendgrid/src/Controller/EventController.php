<?php

namespace Drupal\mla_sendgrid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EventController.
 */
class EventController extends ControllerBase {

  /**
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function ical(NodeInterface $node) {
    if ($node->getType() != 'event') {
      return $this->redirect('entity.node.canonical', ['node' => $node->id()]);
    }

    $startDate = $node->field_event_date_time->start_date->format('Y-m-d\TH:i:sP', ['timezone' => 'America/Vancouver']);
    $endDate = $node->field_event_date_time->end_date->format('Y-m-d\TH:i:sP', ['timezone' => 'America/Vancouver']);

    $location = [
      $node->field_event_venue->value,
      $node->field_event_location->address_line1,
      $node->field_event_location->locality,
      $node->field_event_location->administrative_area,
      $node->field_event_location->postal_code,
      $node->field_event_location->country_code,
    ];

    $location = array_filter($location);

    $vCalendar = new \Eluceo\iCal\Component\Calendar('mlacanada.com');
    $vEvent = new \Eluceo\iCal\Component\Event();
    $vEvent
      ->setDtStart(new \DateTime($startDate))
      ->setDtEnd(new \DateTime($endDate))
      ->setSummary($node->getTitle());
    $vEvent->setLocation(implode(', ', $location));
    $vCalendar->addComponent($vEvent);

    $output = new Response();

    $output->headers->set('Content-Type', 'text/calendar; charset=utf-8');
    $output->headers->set('Content-Disposition', 'attachment; filename="event-' . $node->id() . '.ics"');

    $output->setContent($vCalendar->render());
    return $output;
  }

}
