<?php

namespace Drupal\mla_picatic\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\RequeueException;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  public function eventPage($event) {
//    return [
//      '#theme' => 'mla_picatic',
//      '#event' => $this->dummyData(),
//    ];


    $config = \Drupal::config('mla_picatic.settings');

    $baseUrl    = $config->get('api_url') . '/event';
    $query      = [
      'filter[user_id]' => $config->get('account_id'),
      'filter[slug]'    => $event,
      'page[limit]'     => 1,
      'page[offset]'    => 0,
      'include'         => 'region,country',
    ];
    $requestUrl = $baseUrl . '?' . http_build_query($query);

    $headers = [
      'Cache-Control' => 'no-cache',
      'Authorization' => 'Bearer ' . $config->get('api_key'),
    ];

    try {
      $client  = \Drupal::httpClient();
      $request = $client->get($requestUrl, ['headers' => $headers]);

      if ($request->getStatusCode() != 200) {
        \Drupal::logger('mla_picatic')
               ->warning('API responded with code %code -- @reason', [
                 '%code'   => $request->getStatusCode(),
                 '@reason' => $request->getReasonPhrase(),
               ]);
        return $this->redirect('<front>');
      }

      $eventData = $this->formatData($request);

      if (!$eventData) {
        return $this->redirect('<front>');
      }
      return [
        '#theme' => 'mla_picatic',
        '#event' => $eventData,
        '#cache' => ['max-age' => 0]
      ];
    } catch (RequeueException $e) {
      watchdog_exception('mla_picatic', $e,
        'Error while trying to connect to API.');
      return $this->redirect('<front>');
    }
  }

  private function formatData($serverResponse) {
    $response = $serverResponse->getBody()->getContents();
    $data       = json_decode($response);

    if (empty($data->data)) {
      return false;
    }

    $fields     = $data->data[0]->attributes;
    $references = [];

    foreach ($data->included as $include) {

      switch ($include->type) {
        case "country":
          $references['country'] = $include->attributes->country;
          break;

        case "region":
          $references['region'] = $include->attributes->iso;
          break;
      }
    }


    $date = [
      'end_date'   => $fields->end_date,
      'end_time'   => $fields->end_time,
      'start_date' => $fields->start_date,
      'start_time' => $fields->start_time,
      'time_zone'  => $fields->time_zone,
    ];
    
    $dates = $this->getDateString($date);

    $eventData = [
      'id'          => $data->data[0]->id,
      'fields'      => $fields,
      'data'        => $data,
      'title'       => $fields->title,
      'image'       => $fields->cover_image_uri,
      'closed'      => $fields->status == 'closed',
      'summary'     => $fields->summary,
      'date_short'  => $dates['short'],
      'date_long'   => $dates['long'],
      'dates'       => $dates,
      'date'        => $date,
      'location'    => [
        'name'    => $fields->venue_name,
        'city'    => $fields->venue_locality,
        'street'  => $fields->venue_street,
        'region'  => $references['region'],
        'country' => $references['country'],
      ],
      'description' => strip_tags($fields->description),
      'disclaimer' => $fields->disclaimer,
    ];

    return $eventData;

  }

  private function getDateString($date) {
    $formattedDates = [];

    $start_date = strtotime($date['start_date'] . 'T' . $date['start_time']);
    $end_date   = strtotime($date['end_date'] . 'T' . $date['end_time']);

    $formattedDates['date_day'] = date('l F j, Y',$start_date);
    $formattedDates['date_time'] = date('g:i A', $start_date).' - '.date('g:i A', $end_date);

    $formattedDates['short'] = date('F j, Y', $start_date);

    $formattedDates['long'] = date('l F j Y h:iA', $start_date) . " - ";

    if ($date['start_date'] != $date['end_date']) {
      $formattedDates['long'] .= date('l F j Y h:iA', $end_date);
    }
    else {
      $formattedDates['long'] .= date('h:iA', $end_date);
    }

    return $formattedDates;
  }

  private function dummyData() {
    $date  = [
      'end_date'   => '2017-07-29',
      'end_time'   => '00:00:00',
      'start_date' => '2017-07-29',
      'start_time' => '17:30:00',
      'time_zone'  => 'America/Vancouver',
    ];
    $dates = $this->getDateString($date);
    return [
      'id'          => 114782,
      'title'       => 'Summer Celebration',
      'image'       => 'https://picatic.global.ssl.fastly.net/img/default-cover-images/cover-photo-7.jpg',
      'closed'      => FALSE,
      'summary'     => 'MLA SUMMER CELEBRATION | HORDERVES + FIREWORKS',
      'date_short'  => $dates['short'],
      'date_long'   => $dates['long'],
      'location'    => [
        'name'    => 'Cardero\'s Dock',
        'city'    => 'Vancouver',
        'street'  => '1525 Coal Harbour Quay',
        'region'  => 'BC',
        'country' => 'Canada',
      ],
      'description' => 'Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Sed posuere consectetur est at lobortis.',
    ];
  }

}
