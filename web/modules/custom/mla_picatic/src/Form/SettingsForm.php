<?php

namespace Drupal\mla_picatic\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mla_picatic.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mla_picatic.settings');
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Enter the Picatic API key.'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#description' => $this->t('Enter the base URL for the Picatic API.'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('api_url'),
    ];
    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Picatic Account ID'),
      '#description' => $this->t('Enter the Picatic account ID.'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('account_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('mla_picatic.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_url', trim($form_state->getValue('api_url'), '/'))
      ->set('account_id', $form_state->getValue('account_id'))
      ->save();
  }

}
