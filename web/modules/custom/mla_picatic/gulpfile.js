var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var watch = require('gulp-watch');
var shell = require('gulp-shell');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat-multi');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var fs = require("fs");
var runSequence = require('run-sequence');

/**
 * This task generates CSS from all SCSS files and compresses them down.
 */
gulp.task('sass', function () {
  return gulp.src([
      './scss/**/*.scss',
    ])
    .pipe(sassGlob())
    .pipe(sourcemaps.init())
    .pipe(sass({
      noCache: true,
      outputStyle: "compressed",
      lineNumbers: false,
      loadPath: './css/*',
      sourceMap: true
    })).on('error', function(error) {
      gutil.log(error);
      this.emit('end');
    })
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css'))
    ;
});

/**
 * This task minifies javascript in the js/js-src folder and places them in the js directory.
 */
gulp.task('js', function() {
  concat({
    'global.js': 'js/**/*.js',
  })
    .pipe(uglify())
    .pipe(gulp.dest('js'))
    // .pipe(notify({
    //   title: "JS Compiled",
    //   message: "All JS files have been recompiled and minified.",
    //   onLast: true
    // }))
    ;
});




/**
 * Defines the watcher task.
 */
gulp.task('watch', function() {
  gulp.watch(['scss/**/*.scss'], { usePolling: true }, ['sass',]);
  // gulp.watch(['templates/**/*.js'], { usePolling: true }, ['js',]);
});

gulp.task('default', ['watch']);