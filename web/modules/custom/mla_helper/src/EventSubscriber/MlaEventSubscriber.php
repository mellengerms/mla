<?php

namespace Drupal\mla_helper\EventSubscriber;

/**
 * @file
 * Contains \Drupal\cb\EventSubscriber\MyEventSubscriber.
 */

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Routing\RouteMatch;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;

/**
 * Event Subscriber MlaEventSubscriber.
 */
class MlaEventSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Constructs a new redirect subscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(AccountInterface $account) {
    $this->account = $account;
  }

  /**
   * Trigger this method when receiving an exception.
   */
  public function onException($event) {
    $exception = $event->getException();

    if ($exception instanceof AccessDeniedHttpException) {
      $route_match = RouteMatch::createFromRequest($event->getRequest());
      $node = $route_match->getParameter('node');
      if ($node instanceof Node && $node->bundle() === 'book') {
        // The node ID is set in the code because there is a fatal error if you set the Access denied page on the Culture page.
        $redirect_url = Url::fromRoute('entity.node.canonical', ['node' => 1370], ['query' => \Drupal::destination()->getAsArray(), 'absolute' => TRUE]);
      }

      if (!empty($redirect_url)) {
        $event->setResponse(new RedirectResponse($redirect_url->toString()));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Set a weight more important that the core for the user.register redirection.
    $events[KernelEvents::EXCEPTION][] = ['onException', 150];
    return $events;
  }

}
