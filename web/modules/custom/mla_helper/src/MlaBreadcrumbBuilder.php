<?php

/**
 * @file
 * Contains Drupal\mla_helper\MlaBreadcrumbBuilder.
 */

namespace Drupal\mla_helper;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;

/**
 * Class MlaBreadcrumbBuilder.
 *
 * @package Drupal\mla_helper
 */
class MlaBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route = $route_match->getCurrentRouteMatch();

    if ($route->getRouteName() === 'paragraphs_edit.edit_form') {
      return TRUE;
    }


    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['route']);

    // Add logic here that builds up the breadcrumbs based on desired behaviour.
    $links[] = Link::createFromRoute(t('Home'), '<front>');

    $node = \Drupal::request()->get('root_parent');

    if ($node instanceof Node && $node->bundle() === 'book') {
      $book = $node->book;
      if (!empty($book)) {
        $bid = $book['bid'];
        $book_entity = Node::load($bid);
        if (!empty($book_entity)) {
          $links[] = Link::createFromRoute($book_entity->label(), 'book.admin_edit', ['node' => $book_entity->id()]);
        }
      }

      $links[] = Link::createFromRoute($node->label(), 'entity.node.edit_form', ['node' => $node->id()]);
    }

    $breadcrumb->setLinks($links);

    return $breadcrumb;
  }

}