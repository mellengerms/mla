<?php

namespace Drupal\mla_helper\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'NewsfeedSwitcherBlock' block.
 *
 * @Block(
 *  id = "newsfeed_switcher_block",
 *  admin_label = @Translation("Newsfeed switch"),
 * )
 */
class NewsfeedSwitcherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $build['#theme'] = 'newsfeed_switcher_block';
    $build['#links'][] = Link::createFromRoute('Latest', 'view.news_reports.page_1');

    $categories = $termStorage->loadTree('category');
    foreach ($categories as $category) {
      if ($this->hasArticles($category->tid) && $category->name !== 'Careers') {
        $sanitized = str_replace('/', '-', str_replace(' ', '-', strtolower($category->name)));
        $build['#links'][] = Link::createFromRoute($category->name, 'view.news_reports.page_2', ['category' => $sanitized]);
      }
    }

    return $build;
  }

  /**
   * Function to know if a category has articles or not.
   *
   * @param [type] $term_id
   * @return boolean
   */
  public function hasArticles($term_id) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'article')
      ->condition('field_category', $term_id)
      ->condition('status', 1)
      ->range(0, 1);
    $results = $query->execute();
    return !empty($results);
  }

}
