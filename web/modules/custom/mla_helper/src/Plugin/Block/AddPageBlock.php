<?php

namespace Drupal\mla_helper\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'AddPageBlock' block.
 *
 * @Block(
 *  id = "add_page_block",
 *  admin_label = @Translation("Add page block"),
 * )
 */
class AddPageBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $route = \Drupal::routeMatch()->getRouteName();

    if ($route == 'book.admin_edit') {
      $node = \Drupal::routeMatch()->getParameter('node');
      $query = \Drupal::destination()->getAsArray();
      $query['parent'] = $node->id();

      $build['link'] = [
        '#attributes' => [
          'class' => [
            'action-links',
          ],
        ],
        '#theme' => 'menu_local_action',
        '#link' => array(
          'title' => $this->t('Add page'),
          'url' => Url::fromRoute('node.add', ['node_type' => 'book'], [
            'query' => $query,
          ]),
        ),
      ];
    }

    if ($route == 'entity.node.canonical') {
      $node = \Drupal::routeMatch()->getParameter('node');

      if (isset($node->book)) {
        $query = \Drupal::destination()->getAsArray();
        $query['parent'] = $node->book['bid'];

        $build['link'] = [
          '#attributes' => [
            'class' => [
              'action-links',
            ],
          ],
          '#theme' => 'menu_local_action',
          '#link' => array(
            'title' => $this->t('Add page'),
            'url' => Url::fromRoute('node.add', ['node_type' => 'book'], [
              'query' => $query,
            ]),
          ),
        ];
      }


    }


    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
